require('dotenv').config()

const express = require('express')
const app = express()
const morgan = require('morgan')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')
const booksRouter = require('./app/books/book.router')
const { handleError } = require('./app/helpers/error')

mongoose.connect(process.env.MONGO_URL, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false })
mongoose.set('debug', process.env.NODE_ENV !== 'production')
const db = mongoose.connection
db.on('error', error => console.error(error))
db.once('open', () => console.log('Connected to Mongoose'))

app.use(morgan('dev'))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: 'false' }))

app.use('/', booksRouter)

app.use((err, req, res, next) => {
  handleError(err, res)
})

module.exports = app
