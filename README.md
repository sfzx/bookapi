# Book api express project
## Prerequisites
node version 12 or higher installed

## Installation
```
npm i
```

## Start
```
npm run start
```

## Running tests
```
npm test
```

# With docker 
## Start
```
docker-compose up -d

```

## Postman documentation

https://documenter.getpostman.com/view/11813283/T1DqfcDW


## Technologies used
```
Express, mongoose, joi, morgan, jest, sinon, chai, supertest, docker
```