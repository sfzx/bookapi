const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi)
const { ErrorHandler } = require('../helpers/error')
const { Types } = require('mongoose')
const bookService = require('./book.service')

const addBookSchema = Joi.object().keys({
  _id: Joi.string(),
  bookName: Joi.string().required(),
  authorName: Joi.string().required(),
  description: Joi.string().required(),
  pageCount: Joi.number().min(2).required()
}).required()

const updateBookSchema = Joi.object().keys({
  bookName: Joi.string(),
  authorName: Joi.string(),
  description: Joi.string(),
  pageCount: Joi.number().min(2)
}).required()

function validateId (bookId, next) {
  try {
    if (!Types.ObjectId.isValid(bookId)) {
      throw new ErrorHandler(400, 'Incorrect id format')
    }
  } catch (error) {
    next(error)
  }
}

async function validateBody (data, schema, next) {
  try {
    await schema.validate(data)
  } catch (e) {
    next(new ErrorHandler(400, e.details[0].message))
  }
}

async function validateExistence (bookId, next) {
  try {
    if (await bookService.getBook(bookId) === null) {
      throw new ErrorHandler(404, 'Item does not exist')
    }
  } catch (error) {
    next(error)
  }
}

const validator = {
  addBook: async (req, res, next) => {
    await validateBody(req.body, addBookSchema, next)
    await next()
  },
  updateBook: async (req, res, next) => {
    await validateId(req.params.bookId, next)
    await validateExistence(req.params.bookId, next)
    await validateBody(req.body, updateBookSchema, next)
    await next()
  },
  deleteBook: async (req, res, next) => {
    await validateId(req.params.bookId, next)
    await validateExistence(req.params.bookId, next)
    await next()
  },
  getBook: async (req, res, next) => {
    await validateId(req.params.bookId, next)
    await validateExistence(req.params.bookId, next)
    await next()
  }
}

module.exports = validator
