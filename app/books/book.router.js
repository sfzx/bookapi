const express = require('express')
const router = express.Router()
const validator = require('./book.validation')
const bookService = require('./book.service')

router.post('/add', validator.addBook, async (req, res) => {
  await bookService.addBook(req.body)
  res.send('New book created')
})

router.get('/books', async (req, res) => {
  const books = await bookService.getAllBooks(req.query.page || 1, req.query.limit || 10)
  res.send(books)
})

router.get('/books/:bookId', validator.getBook, async (req, res) => {
  const book = await bookService.getBook(req.params.bookId)
  res.send(book)
})

router.put('/update/:bookId', validator.updateBook, async (req, res) => {
  await bookService.updateBook(req.params.bookId, req.body)
  res.send('All updated')
})

router.delete('/delete/:bookId', validator.deleteBook, async (req, res) => {
  await bookService.deleteBook(req.params.bookId)
  res.send('Book deleted')
})

module.exports = router
