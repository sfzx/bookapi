const Book = require('./book.model')

const bookService = {
  addBook: async (book) => {
    const newBook = await new Book(book)
    return newBook.save()
  },
  getAllBooks: (page, limit) => {
    return Book.find({}, { __v: 0 }).skip(page * limit).limit(Number(limit))
  },
  getBook: (bookID) => {
    return Book.findById(bookID, { __v: 0 })
  },
  updateBook: (bookID, body) => {
    return Book.findByIdAndUpdate(bookID, { body })
  },
  deleteBook: (bookID) => {
    return Book.deleteOne({ _id: bookID })
  }
}

module.exports = bookService
