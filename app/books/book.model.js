const mongoose = require('mongoose')

const bookModelSchema = new mongoose.Schema({
  bookName: {
    type: String,
    required: true
  },
  authorName: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  pageCount: {
    type: Number,
    required: true
  },
  createAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date,
    required: true,
    default: Date.now
  }
})

bookModelSchema.pre('findOneAndUpdate', function () {
  this.set({ updatedAt: new Date() })
})

module.exports = mongoose.model('Book', bookModelSchema)
