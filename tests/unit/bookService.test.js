const sinon = require('sinon')
const chai = require('chai')
const mongoose = require('mongoose')
const bookService = require('../../app/books/book.service')
const expect = chai.expect

describe('UNIT bookService', () => {
  const bookId = '5f10a40204bc4d327c1d480f'
  const data = {
    _id: bookId,
    bookName: 'new book',
    authorName: 'new author',
    description: 'Description text for new book',
    pageCount: 256
  }
  it('Should call `bookService.addBook`', (done) => {
    const saveStub = sinon.stub(mongoose.Model.prototype, 'save')
    const addBookSpy = sinon.spy(bookService, 'addBook')
    addBookSpy(data)
    setTimeout(() => {
      expect(addBookSpy.calledWithExactly(data)).to.equal(true)
      expect(saveStub.callCount).to.equal(1)
      done()
    }, 0)
  })

  it('Should call `bookService.getAllBooks` and return data', () => {
    const limitSpy = sinon.spy()
    const skipStub = sinon.stub().returns({ limit: limitSpy })
    const findStub = sinon.stub(mongoose.Model, 'find').returns({ skip: skipStub })

    bookService.getAllBooks(2, 5)
    expect(findStub.callCount).to.equal(1)
    expect(skipStub.calledWithExactly(10)).to.equal(true)
    expect(limitSpy.calledWithExactly(5)).to.equal(true)
  })

  it('Should call `bookService.getBook` and return data', () => {
    const findByIdStub = sinon.stub(mongoose.Model, 'findById')
    const getBookSpy = sinon.spy(bookService, 'getBook')
    findByIdStub.returns(data)
    getBookSpy(bookId)
    expect(getBookSpy.calledWithExactly(bookId)).to.equal(true)
    expect(findByIdStub.callCount).to.equal(1)
    expect(bookService.getBook(bookId)).to.be.an('object')
    expect(bookService.getBook(bookId)).to.equal(data)
  })

  it('Should call `bookService.updateBook` and return data', () => {
    const body = { authorName: 'newest author' }
    const findByIdAndUpdateStub = sinon.stub(mongoose.Model, 'findByIdAndUpdate')
    const updateBookSpy = sinon.spy(bookService, 'updateBook')
    findByIdAndUpdateStub.returns(data)
    updateBookSpy(bookId, body)
    expect(updateBookSpy.calledWithExactly(bookId, body)).to.equal(true)
    expect(findByIdAndUpdateStub.callCount).to.equal(1)
    expect(bookService.updateBook()).to.be.an('object')
    expect(bookService.updateBook()).to.equal(data)
  })

  it('Should call `bookService.deleteBook` and return data', () => {
    const deleteOneStub = sinon.stub(mongoose.Model, 'deleteOne')
    const deleteBookSpy = sinon.spy(bookService, 'deleteBook')
    deleteBookSpy(bookId)
    expect(deleteBookSpy.calledWithExactly(bookId)).to.equal(true)
    expect(deleteOneStub.callCount).to.equal(1)
  })
})
