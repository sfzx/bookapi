const request = require('supertest')
const app = require('../../app')
const mongoose = require('mongoose')
const bookService = require('../../app/books/book.service')

const bookId = '5f10a40204bc4d327c1d480f'
const nonExistentID = '5f10a40204bc4d399c1d4809'

const newBook = {
  _id: bookId,
  bookName: 'new book',
  authorName: 'new authr',
  description: 'Description text for new book',
  pageCount: 256
}
const incorrectBook = {
  bookame: 'new book',
  authorName: 'new authr',
  description: 'Description text for new book',
  pageCount: 122
}

describe('Bookapi integration test:', () => {
  afterAll(async () => {
    mongoose.connection.close()
  })

  beforeEach(async () => {
    await bookService.addBook(newBook)
  })

  afterEach(async () => {
    await bookService.deleteBook(bookId)
  })

  // Testing POST /add rout

  test('On POST /add should create new book', async () => {
    const res = await request(app)
      .post('/add')
      .send({
        _id: '5f10a40204bc4d399c1d4809',
        bookName: 'boku no pico',
        authorName: 'pico',
        description: 'Description text for new book',
        pageCount: 256
      })
    expect(res.statusCode).toEqual(200)
    expect(res.text).toBe('New book created')
    await bookService.deleteBook('5f10a40204bc4d399c1d4809')
  })

  test('On POST /add should return an error if incorrect input', async () => {
    const res = await request(app)
      .post('/add')
      .send(incorrectBook)
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toBe('"bookName" is required')
  })

  // Testing GET /books rout

  test('On GET /books should return all books', async () => {
    const res = await request(app)
      .get('/books')
    expect(res.statusCode).toEqual(200)
  })

  // Testing GET /books/:bookId rout

  test('On GET /books/:bookId should return book', async () => {
    const res = await request(app)
      .get('/books/' + bookId)
    expect(res.statusCode).toEqual(200)
    expect(res.body).toMatchObject(newBook)
  })

  test('On GET /books/:bookId should return error if id format is incorrect', async () => {
    const res = await request(app)
      .get('/books/' + 'incorrectId')
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toBe('Incorrect id format')
  })

  test('On GET /books/:bookId should return error if there are no book with such id', async () => {
    const res = await request(app)
      .get('/books/' + nonExistentID)
    expect(res.statusCode).toEqual(404)
    expect(res.body.message).toBe('Item does not exist')
  })

  // Testing PUT /update/:bookId rout

  test('On PUT /update/:bookId should update book', async () => {
    const res = await request(app)
      .put('/update/' + bookId)
      .send({
        bookName: 'boku no pico',
        authorName: 'pico'
      })
      .set('Accept', 'application/json')
    expect(res.statusCode).toEqual(200)
    expect(res.text).toBe('All updated')
  })

  test('On PUT /update/:bookId should return error if id format is incorrect', async () => {
    const res = await request(app)
      .put('/update/' + 'incorrectId')
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toBe('Incorrect id format')
  })

  test('On PUT /update/:bookId should return error if there are no book with such id', async () => {
    const res = await request(app)
      .put('/update/' + nonExistentID)
    expect(res.statusCode).toEqual(404)
    expect(res.body.message).toBe('Item does not exist')
  })

  test('On POST /update/:bookId should return an error if incorrect input', async () => {
    const res = await request(app)
      .put('/update/' + bookId)
      .send(incorrectBook)
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toBe('"bookame" is not allowed')
  })

  // Testing DELETE /delete/:bookId rout

  test('On DELETE /delete/:bookId should delete book', async () => {
    const res = await request(app)
      .delete('/delete/' + bookId)
    expect(res.statusCode).toEqual(200)
    expect(res.text).toBe('Book deleted')
  })

  test('On DELETE /delete/:bookId should return error if id format is incorrect', async () => {
    const res = await request(app)
      .del('/delete/' + 'incorrectId')
    expect(res.statusCode).toEqual(400)
    expect(res.body.message).toBe('Incorrect id format')
  })

  test('On DELETE /delete/:bookId should return error if there are no book with such id', async () => {
    const res = await request(app)
      .delete('/delete/' + nonExistentID)
    expect(res.statusCode).toEqual(404)
    expect(res.body.message).toBe('Item does not exist')
  })
})
